
function binarySearch(arr, key, left = null, right = null) {
    if (!left && !right) {
        left = 0;
        right = arr.length;
    }

    if (right < left) {
        throw new Error("Array bounderies are wrong");
    } else if (left === right) {
        return null;
    }

    let middle = left + Math.floor((right - left) / 2);

    if (arr[middle] === key) {
        if (arr[middle - 1] === arr[middle]) {
            do {
                middle--;
            } while (arr[middle - 1] === arr[middle]);
        }

        return middle;
    } else if (arr[middle] < key) {
        const l = arr[middle] < arr[middle + 1] ? middle + 1 : left;
        const r = arr[middle] < arr[middle + 1] ? right : middle;
        return binarySearch(arr, key, l, r);
    } else if (arr[middle] > key) {
        const l = arr[middle] < arr[middle + 1] ? left : middle + 1;
        const r = arr[middle] < arr[middle + 1] ? middle : right;
        return binarySearch(arr, key, l, r);
    }
}

module.exports = { binarySearch };