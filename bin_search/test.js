const expect = require('expect.js');
const { binarySearch } = require('./bin_search');

describe("Binary search", () => {
    it('Index of 5 in array [0, 1, 2, 3, 4, 5, 6] must be 5', () => {
        const arr = [ 0, 1, 2, 3, 4, 5, 6 ];
        const result = binarySearch(arr, 5);

        expect(result).to.equal(5);
    });

    it('Index of 34 in array [0, 15, 34, 34, 34, 72, 100] must be 2', () => {
        const arr = [0, 15, 34, 34, 34, 72, 100];
        const result = binarySearch(arr, 34);

        expect(result).to.equal(2);
    });

    it('Index of 34 in array [0, 15, 16, 17, 18, 72, 100] must be null', () => {
        const arr = [0, 15, 16, 17, 18, 72, 100];
        const result = binarySearch(arr, 34);

        expect(result).to.equal(null);
    });

    it('Index of 5 in array [6, 5, 4, 3, 2, 1, 0] must be 1', () => {
        const arr = [6, 5, 4, 3, 2, 1, 0];
        const result = binarySearch(arr, 5);

        expect(result).to.equal(1);
    });
});