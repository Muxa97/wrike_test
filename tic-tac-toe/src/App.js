import React, { Component } from 'react';
import './App.css';

import { Store, initialStore } from './Store/Store';

import Header from './Modules/Header/Header';
import Field from './Modules/Field/Field';
import Modal from './Modules/Modal/Modal';

class App extends Component {
    constructor(props) {
        super(props);

        this.changeCellState = (id) => {
            const field = [
                [ ...this.state.fieldState[0] ],
                [ ...this.state.fieldState[1] ],
                [ ...this.state.fieldState[2] ]
            ];

            if (field[id[0]][id[1]] !== '') {
                return;
            }

            const newTurn = this.state.currentTurn === 'X' ? 'O' : 'X';

            field[id[0]][id[1]] = this.state.currentTurn;

            this.setState(state => ({
                    ...state,
                    fieldState: field,
                    currentTurn: newTurn
                })
            );

            setTimeout(() => {
                const horizontals = this.state.fieldState;
                const vertical = [];
                const diagonals = [ [], [] ];
                for (let i = 0; i < 3; i++) {
                    this.checkArray(horizontals[i]);

                    for (let j = 0; j < 3; j++) {
                        vertical.push(field[j][i]);
                    }
                    this.checkArray(vertical);
                    vertical.length = 0;

                    diagonals[0].push(field[i][i]);
                    diagonals[1].push(field[2 - i][i]);
                }
                
                this.checkArray(diagonals[0]);
                this.checkArray(diagonals[1]);
                diagonals[0].length = 0;
                diagonals[1].length = 0;
            }, 500);
        };

        this.clearField = () => {
            this.setState(state => ({
                ...state,
                fieldState: [
                    ['', '', ''],
                    ['', '', ''],
                    ['', '', '']
                ],
                currentTurn: initialStore.currentTurn,
            }));
        };


        this.state = {
            score: [ ...initialStore.score ],
            currentTurn: initialStore.currentTurn,
            fieldState: [
                ['', '', ''],
                ['', '', ''],
                ['', '', '']
            ],
            modalIsOpen: false,
            winner: 0,

            changeCellState: this.changeCellState,
            clearField: this.clearField
        };
    }

    checkArray(arr) {
        let winner = 0;

        if (arr.every(cell => cell === 'X')) {
            winner = 1;
        } else if (arr.every(cell => cell === 'O')) {
            winner = 2;
        }

        if (winner) {
            const score = winner === 1 ? [
                this.state.score[0] + 1, this.state.score[1]
                ] : [
                this.state.score[0], this.state.score[1] + 1
            ];

            this.setState(state => ({
                ...state,
                score: score,
                modalIsOpen: true,
                winner: winner
            }));
        }
    }

    closeModal() {
        this.setState( state => ({
            score: [ ...state.score ],
            currentTurn: initialStore.currentTurn,
            fieldState: [
                ['', '', ''],
                ['', '', ''],
                ['', '', '']
            ],
            modalIsOpen: false,
            winner: 0,

            changeCellState: this.changeCellState
        }));
    }

    render() {
        return (
            <Store.Provider value={this.state}>
                <main className="app">
                    <Header />
                    <Field />
                    { this.state.modalIsOpen &&
                    <Modal playerID={this.state.winner} closeModal={ () => this.closeModal() }/> }
                </main>
            </Store.Provider>
        );
    }
}

export default App;
