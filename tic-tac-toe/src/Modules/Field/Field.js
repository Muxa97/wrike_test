import React, { Component } from 'react';
import './Field.css';

import { Store } from '../../Store/Store';


class Cell extends Component {
    constructor(props) {
        super(props);

        this.state = {
            cell_id: props.cell_id
        }
    }


    render() {
        return (
            <Store.Consumer>
                {({ fieldState, changeCellState }) => (
                    <div className="cell" id={this.state.cell_id} onClick={() => changeCellState(this.state.cell_id)}>
                        { fieldState[this.state.cell_id[0]][this.state.cell_id[1]] }
                    </div>
                )}
            </Store.Consumer>
        )
    }
}

function Field() {
    const ids = [];
    for (let i = 0; i < 3; i++) {
        for (let j = 0; j < 3; j++) {
            ids.push([i, j]);
        }
    }

    return (
        <div className="field">
            {ids.map(id => (
                <Cell key={`cell-${id[0]}${id[1]}`} cell_id={id} />
            ))}
        </div>
    )
}

export default Field;