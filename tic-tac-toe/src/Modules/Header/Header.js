import React from 'react';
import { Store } from '../../Store/Store';
import './Header.css';

function Header() {
    return (
        <Store.Consumer>
            { ({score, currentTurn, clearField}) => (
                <>
                    <header className="app-header">
                        <h1>Score</h1>
                        <div className="app-header__score">
                            <span className={
                                `score__player-label ${currentTurn === 'X' ? 'active' : ''}`
                            }>(X) First player</span>
                            <span className="score__table">{`${score[0]} : ${score[1]}`}</span>
                            <span className={
                                `score__player-label ${currentTurn === 'O' ? 'active' : ''}`
                            }>Second player (O)</span>
                        </div>
                        <button onClick={clearField}>Clear field</button>
                    </header>
                </>
            )}
        </Store.Consumer>
    );
}

export default Header;