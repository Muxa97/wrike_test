import React from 'react';
import './Modal.css';

function Modal({ playerID, closeModal }) {
    return (
        <div className="modal">
            <span>{ playerID === 1 ? "First" : "Second" } player win!</span>
            <button className="modal__closeBtn" onClick={closeModal}>Close</button>
        </div>
    )
}

export default Modal;