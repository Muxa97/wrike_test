import React from 'react';

export const initialStore = {
    score: [0, 0],
    currentTurn: 'X',
    fieldState: [
        ['', '', ''],
        ['', '', ''],
        ['', '', '']
    ],

    changeCellState: () => {},
    clearField: () => {}
};

export const Store = React.createContext(initialStore);